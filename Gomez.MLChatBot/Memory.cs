﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Gomez.MLChatBot
{
    public class MemoryData
    {
        public MemoryData()
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Words = new List<string>();
        }

        public MemoryData(string[] words) : this()
        {
            Words = words;
        }

        public IEnumerable<string> Words { get; set; }
        public string Id { get; set; }

        public override string ToString()
        {
            if (Words.Count() == 0) return "";
            return String.Join(" ", Words);
        }


        private IEnumerable<int> _Loudness()
        {
            foreach (var word in Words)
            {
                yield return word.Skip(1).Count(c => char.IsUpper(c)) + word.Count(f => f == '!');
            }
        }

        [JsonIgnore]
        public int Loudness
        {
            get
            {
                return _Loudness().Sum();
            }
        }

        [JsonIgnore]
        public bool IsQuestion
        {
            get
            {
                if (Words.Count() == 0) return false;
                return Words.Last().TrimEnd().Last() == '?';
            }
        }

        [JsonIgnore]
        public bool IsThinking
        {
            get
            {
                if (Words.Count() == 0) return false;
                return Words.Last().Count(x => x == '.') > 2;
            }
        }

        //How do we capture Sadness, Happiness or will it automatic be formed by the word relationship algorithm?
        //Need emotions properties to filter by
        //Here
    }

    public class MemoryRelation
    {
        public MemoryRelation()
        {
            Id = Guid.NewGuid().ToString().ToUpper();
        }

        public string Id { get; set; }

        public string InputId { get; set; }
        public string OutputId { get; set; }

        [JsonIgnore]
        private MemoryData _input;
        [JsonIgnore]
        public virtual MemoryData Input
        {
            get
            {
                if(String.IsNullOrEmpty(InputId) || MemmoryRepository.Current == null)
                {
                    return null; 
                }

                if(_input == null)
                {
                    _input = MemmoryRepository.Current.Data.FirstOrDefault(x => x.Id == InputId);
                }

                return _input;
            }
        }

        [JsonIgnore]
        private MemoryData _output;
        [JsonIgnore]
        public virtual MemoryData Output
        {
            get
            {
                if (String.IsNullOrEmpty(OutputId) || MemmoryRepository.Current == null)
                {
                    return null;
                }

                if (_output == null)
                {
                    _output = MemmoryRepository.Current.Data.FirstOrDefault(x => x.Id == OutputId);
                }

                return _output;
            }
        }
    }

    public class MemmoryRepository
    {
        public static MemmoryRepository Current { get; set; }

        public MemmoryRepository()
        {
            Data = new List<MemoryData>();
            Relations = new List<MemoryRelation>();
            Current = this;
        }

        [JsonIgnore]
        public string LastMemoryInputId { get; set; }

        [JsonIgnore]
        public string[] LastWords { get; set; }

        public  IList<MemoryData> Data { get; set; }
        public  IList<MemoryRelation> Relations { get; set; }
    }

    public class Memory
    {
        private MemmoryRepository MemmoryRepository { get; set; }

        public long LongCount()
        {
            return MemmoryRepository.Data.LongCount();
        }

        public Memory()
        {
            MemmoryRepository = new MemmoryRepository();
            Load();
        }

        public void Say(string input)
        {
            if (String.IsNullOrEmpty(input)) return;
            MemmoryRepository.LastWords = input.Split(' ');
        }

        public string Response()
        {
            return FindRelatedMemory(MemmoryRepository.LastWords)?.ToString() ?? "...";
        }

        public void Learn(string input)
        {
            if (String.IsNullOrEmpty(input)) return ;

            var words = input.Split(' ');
            var wordCount = words.Count();
            RegisterMemory(words);
            MemmoryRepository.LastWords = words;
        }

        private void RegisterMemory(string[] words)
        {
            var memories = FindMemories(words);

            bool newInput = false;
            var mInput = memories.FirstOrDefault(x => String.Join(" ", x.Words) == String.Join(" ", words));
            if(mInput == null)
            {
                newInput = true;
                mInput = new MemoryData(words);
            }
           
            if (!String.IsNullOrEmpty(MemmoryRepository.LastMemoryInputId))
            {
                foreach (MemoryData memory in memories)
                {
                    MemmoryRepository.Relations.Add(new MemoryRelation()
                    {
                        InputId = mInput.Id,
                        OutputId = MemmoryRepository.LastMemoryInputId
                    });
                }
            }

            MemmoryRepository.LastMemoryInputId = mInput.Id;
            if(newInput)
            {
                MemmoryRepository.Data.Add(mInput);
            }
        }

        private IEnumerable<MemoryData> FindMemories(string[] words)
        {
            return MemmoryRepository.Data.Where(x => x.Words.Intersect(words).LongCount() > 0)
                .OrderByDescending(x => x.Words.Intersect(words).LongCount()).AsEnumerable();
        }
        
        private IEnumerable<MemoryRelation> FindRelations(string[] words)
        {
            var memories = FindMemories(words);
            var relations = MemmoryRepository.Relations
                .Where(x => memories.Any(z => z.Id == x.InputId));

            return relations = relations.GroupBy(x => x.InputId)
                .OrderByDescending(x => x.LongCount())
                .SelectMany(x => x);
        }

        private MemoryData FindRelatedMemory(string[] words)
        {
            var relations = FindRelations(words);


            //Need improvement to understand situations by picking right relation
            var relation = relations
                .Where
                (   
                    x => 
                    x.Input.Loudness <= x.Output.Loudness && 
                    x.Input.IsQuestion != x.Output.IsQuestion &&
                    x.Input.IsThinking == x.Output.IsQuestion
                )
                .FirstOrDefault();

            if(relation == null)
            {
                relation = relations.FirstOrDefault();
            }

            if (relation != null)
            {
                return relation.Output;
            }

            return null;
        }

        private void Load()
        {
            string filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "data.json");
            if(File.Exists(filePath))
            {
                using (StreamReader r = new StreamReader(filePath))
                {
                    string jsonStr = r.ReadToEnd();
                    MemmoryRepository = JsonConvert.DeserializeObject<MemmoryRepository>(jsonStr);
                }
            }
        }

        public void Save()
        {
            string filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "data.json");
            using (StreamWriter file = File.CreateText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, MemmoryRepository);
            }
        }
    }
}
