﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Gomez.MLChatBot
{
    class Program
    {
        public static Memory Memory { get; set; }

        static void Main(string[] args)
        {
            Memory = new Memory();

            //Todo, make a initial list so we don't have make the bot learn everything. 
            if(Memory.LongCount() == 0)
            {
                var textsAndResponses = new List<Tuple<string, string>>(); // Todo get big list from somewhere.
                foreach (Tuple<string, string> tuple in textsAndResponses)
                {
                    Memory.Learn(tuple.Item1);
                    Memory.Learn(tuple.Item2);
                }
            }

            do
            {
                while (!Console.KeyAvailable)
                {
                    Think();
                    Thread.Sleep(200);
                }

                Thread.Sleep(200);
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        static void Think()
        {
            Console.Write("Question: ");
            string input = Console.ReadLine();

            Memory.Learn(input);
            Console.WriteLine("Machine: {0}", Memory.Response());

            Memory.Save();
        }
    }
}
